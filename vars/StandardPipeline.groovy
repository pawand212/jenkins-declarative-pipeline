def call(config) {
	Map modules=[:]
	pipeline {
		agent any
		stages {
		    stage('L1-Approval') {
            parallel {
                stage('L1 TL Approval') {
                    steps {
                        input id: 'TL', message: 'L1 TL Approval', parameters: [booleanParam(defaultValue: false, description: 'given to tick', name: 'tickIt')]
                    }
                }
                stage('L1 QA Approval') {
                    steps {
                        input id: 'QA', message: 'L1 QA Approval', parameters: [booleanParam(defaultValue: false, description: 'given to tick', name: 'tickIt')]
                    }
                }
            }
        }
		    /*stage('Input Stage') {
		        parallel(
		            'L1 TL Approval': {
		                input id: 'TL', message: 'L1 TL Approval', parameters: [booleanParam(defaultValue: false, description: 'given to tick', name: 'tickIt')]
		            },
		            'L1 QA Approval': {
		                input id: 'QA', message: 'L1 QA Approval', parameters: [booleanParam(defaultValue: false, description: 'given to tick', name: 'tickIt')]
		            }
		            )
		    }*/
			stage('Example Build') {
				steps {
					script{
						sh 'pwd'
						sh 'ls -lart'
						modules.utils = new Utils()
						
						data = modules.utils.getRecipients('359728', 'pawankumar_dwivedi@syntelinc.com')
						//load 'Utils.groovy'
						//Utils.getRecipients('3695528,3597283,3579357', 'pawankumar_dwivedi@syntelinc.com')
						echo "Data: ${data}"
					}
				}
			}
		}
	}
}